
package javahisbroker;

/**
 * 
 * @author Oliver, Hannes, Giulia
 */
public class JavaHisBroker {
    private static UDPServer srv; // Hier ist die Klasse UDP Server (ServerObjekt)
    
    /**
     * @param args Broker wird hier gestartet
     */
    public static void main(String[] args) {
        int port;
        
        port = 9876;
        for (int i = 0; i < args.length; i++) {
            if (args[i].charAt(0) != '-') {
                continue;
            }
            
            switch (args[i].charAt(1)) {
                case 'p':
                    if ((i + 1) < args.length) {
                        i++;
                        
                        port = Integer.parseInt(args[i]);
                        if (port < 1 || port > 0xfffe) {
                            System.out.println("bad port number");
                            System.exit(0);
                        }
                        break;
                    }
                    System.out.println("Option -p requires port number");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Bad option " + args[i]);
                    System.exit(0);
            }
        }
        
        srv = new UDPServer(port);
        
        System.out.println("Starting server on port " + port);
        srv.mainLoop();
        
        
    }
}
