package javahisbroker;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oliver, Hannes, Giulia
 */
public class UDPServer {

    protected int port;

    protected byte[] receiveBuffer;
    protected DatagramSocket serverSocket;
    protected List<InetAddress> clients = new LinkedList<InetAddress>();

    /**
     * Konstruktor 1
     */
    public UDPServer() {
        initialize(9876);
    }

    /**
     * Konstruktor 2
     */
    public UDPServer(int port) {
        initialize(port);
    }

    /**
     * Erstellt den Port und die Größe des Paketes?
     *
     * @param port Port
     */
    private void initialize(int port) {
        this.port = port;
        this.receiveBuffer = new byte[1024];
    }

    /**
     * Sendet das Paket an einen Client
     *
     * @param data Daten
     * @param addr Adresse
     * @param dPort Ziel Port
     */
    protected void sendPacket(byte[] data, InetAddress addr, int dPort) {
        DatagramPacket sendPacket = new DatagramPacket(data, data.length, addr, dPort);//dieser "Horcher" erhält das paket und... try
        try {
            serverSocket.send(sendPacket); // ... versucht das paket zu senden
        } catch (IOException e) {
            System.out.println("Send failed: " + e.getMessage());
        }
    }

    /**
     * Diese Methode verarbeitet nach Empfang ein Packet (egal welches).
     *
     * @param pRcv Das Paket was der server bekommen hat.
     */
    protected void processPacket(DatagramPacket pRcv) {
        byte[] data = pRcv.getData(); // Wahrscheinlich reine Information (Unser Objekt)
        int sPort = pRcv.getPort(); // Woher das paket kommt ? (SourcePort)
        InetAddress IPAddress = pRcv.getAddress(); // Wahrscheinlich die IP adress, wo es hingeschickt wird.

        System.out.println("Es kommt ein Paket von: " + IPAddress);

        if (clients.size() < 250) {
            System.out.println("Broker hat noch Kapazität.");
            //Discover
            if (readHeader(data, "dis") == 1 && readHeader(data, "jin") == 0) {
                System.out.println("Ein Discover wurde empfangen.");
                writeHeader(data, "ack", 1);
                sendPacket(data, IPAddress, sPort);
                System.out.println("Broker sendet Discover Confirm\n");
            }
            //Join Confirm vom Client
            if (readHeader(data, "jin") == 1 && readHeader(data, "ack") == 1 && readHeader(data, "dis") == 0) {
                System.out.println("Der Join wurde bestätigt.");
                writeHeader(data, "ack", 1);
                sendPacket(data, IPAddress, sPort);
                System.out.println("Established\n");
                
            }
            //Join
            
            if (readHeader(data, "jin") == 1 && readHeader(data, "dis") == 0) {
                System.out.println("Ein Join wurde empfangen.");
                writeHeader(data, "ack", 1);
                sendPacket(data, IPAddress, sPort);
                System.out.println("Broker sendet Join Confirm\n");
               
            }
            //Leave
            if (readHeader(data, "lev") == 1 && readHeader(data, "dis") == 0) {
                System.out.println("Ein Leave wurde empfangen.");
                writeHeader(data, "ack", 1);
                sendPacket(data, IPAddress, sPort);
                System.out.println("Broker sendet Leave Confirm\n");
            }
            //Datenpaket
            if (readHeader(data, "dat") == 1 && readHeader(data, "dis") == 0) {
                System.out.println("Ein Datenpaket wurde empfangen.");
                writeHeader(data, "ack", 1);
                sendPacket(data, IPAddress, sPort);
                System.out.println("Broker sendet Data Confirm\n");
                byte[] text = trimPacket(data, 2);
                String data_value = new String(text, StandardCharsets.UTF_8);
                System.out.println("Text: " + data_value);

            }
        } else {
            //Decline
            System.out.println("Broker hat keine Kapazität.");
            writeHeader(data, "ack", 0);
            writeHeader(data, "dis", 0);
            writeHeader(data, "jin", 0);
            writeHeader(data, "lev", 0);
            writeHeader(data, "dat", 0);
            sendPacket(data, IPAddress, sPort);
        }
    }

    /**
     * Hier wird der Server gestartet und er erwartet nun Pakete.
     */
    public void mainLoop() {
        try {
            serverSocket = new DatagramSocket(port);
        } catch (SocketException se) {
            serverSocket = null;
        }

        while (true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            try {
                serverSocket.receive(receivePacket);
            } catch (IOException e) {
                System.out.println("IO exception: " + e.getMessage());
                continue;
            }

            processPacket(receivePacket);
        }
    }

    /**
     * Gibt den Port Destinatin zurück aus dem Paket
     *
     * @exception Wenn der Port größer als 9999 ist kann dieser nicht ausgelesen
     * werden.
     * @param packet byte[] Packet
     * @return int Portnummer
     */
    public int getPortDestination(byte[] packet) {
        int ret = 0;
        ret = packet[8] * 100 + packet[9];
        return ret;
    }

    /**
     * Gibt den Port Source zurück aus dem Paket.
     *
     * @exception Wenn der Port größer als 9999 ist kann dieser nicht ausgelesen
     * werden.
     * @param packet byte[] Packet
     * @return int Portnummer
     */
    public static int getPortSource(byte[] packet) {
        int ret = 0;
        ret = packet[10] * 100 + packet[11];
        return ret;
    }

    /**
     * Gibt Urspungs IPAdresse zurück.
     *
     * @param packet Packet aus dem die Adresse gelesen werden soll.
     * @return InetAdress IP
     */
    public static InetAddress getIPSource(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                    (packet[0] & 0xff) + "."
                    + (packet[1] & 0xff) + "."
                    + (packet[2] & 0xff) + "."
                    + (packet[3] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    /**
     * Gibt Ziel IPAdresse zurück.
     *
     * @param packet Packet aus dem die Adresse gelesen werden soll.
     * @return InetAdress IP
     */
    public InetAddress getIPDestination(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                      (packet[4] & 0xff) + "."
                    + (packet[5] & 0xff) + "."
                    + (packet[6] & 0xff) + "."
                    + (packet[7] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    /**
     * Erstellt ein Header für ein Packet.
     *
     * @param ipS InetAddress IP Source
     * @param ipD InetAddress IP Destination
     * @param portS int Port Source (Darf nicht größer 9999 sein)
     * @param portD int Port Destination (Darf nicht größer 9999 sein)
     * @param acknowledge int 0 = kein Ack, 1 = Ack
     * @param discover int 0 = kein dis, 1 = dis
     * @param leave int 0 = kein lev, 1 = lev
     * @param join int 0 = kein jin, 1 = jin,
     * @param type int 0 = kein anycast, 1 = broadcast
     * @param zustellung int 0 = kein zst, 1 = zst
     * @param dataPacket int 0 = kein dat, 1 = dat
     * @return byte[] header
     */
    public byte[] buildHeader(//much wow
            InetAddress ipS,
            InetAddress ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung,
            int dataPacket) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD.getAddress();
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/,
            (byte) dataPacket/*18*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }
     /**
     * Erstellt ein Header für ein Paket.
     *
     * @param ipS InetAddress IP Source
     * @param ipD byte[] IP Destination
     * @param portS int Port Source (Darf nicht größer 9999 sein)
     * @param portD int Port Destination (Darf nicht größer 9999 sein)
     * @param acknowledge int 0 = kein Ack, 1 = Ack
     * @param discover int 0 = kein dis, 1 = dis
     * @param leave int 0 = kein lev, 1 = lev
     * @param join int 0 = kein jin, 1 = jin,
     * @param type int 0 = kein typ, 1 = typ
     * @param zustellung int 0 = kein zst, 1 = zst
     * @param dataPacket int 0 = kein dat, 1 = dat
     * @return byte[] header
     */
    public byte[] buildHeader(
            InetAddress ipS,
            byte[] ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung,
            int dataPacket) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD;
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/,
            (byte) dataPacket/*18*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }
    /**
     * Ließt das Flag des Headers aus.
     * @param dataPacket Datenpaket
     * @param flag Flag welches ausgegeben werden soll. (Es gehen nur: ack,dis,lev,jin,typ,zst,dat)
     * @return  int Wert des Flags (0 = false, 1 = true)
     */
    public int readHeader(byte[] dataPacket, String flag) {
        int ret = 0;

        switch (flag) {
            case "ack":
                int ack = dataPacket[12];
                ret = ack;

                break;
            case "dis":
                int dis = dataPacket[13];
                ret = dis;

                break;
            case "lev":
                int lev = dataPacket[14];
                ret = lev;

                break;
            case "jin":
                int jin = dataPacket[15];
                ret = jin;

                break;
            case "typ":
                int typ = dataPacket[16];
                ret = typ;

                break;
            case "zst":
                int zst = dataPacket[17];
                ret = zst;

                break;
            case "dat":
                int dat = dataPacket[18];
                ret = dat;

                break;
        }
        return ret;
    }
    /**
     * Verändert ein Flag des Datenpaketes
     * @param dataPacket Datenpacket
     * @param flag Flag, welches verändert werden soll
     * @param aendern neuer Wert des Flags 0 = false, 1 = true
     */
    public static void writeHeader(byte[] dataPacket, String flag, int aendern) {
        switch (flag) {
            case "ack":
                dataPacket[12] = (byte) aendern;
                break;
            case "dis":
                dataPacket[13] = (byte) aendern;
                break;
            case "lev":
                dataPacket[14] = (byte) aendern;
                break;
            case "jin":
                dataPacket[15] = (byte) aendern;
                break;
            case "typ":
                dataPacket[16] = (byte) aendern;
                break;
            case "zst":
                dataPacket[17] = (byte) aendern;
                break;
            case "dat":
                dataPacket[18] = (byte) aendern;
                break;
        }
    }
    /**
     * Teilt ein Datenpaket auf und gibt entweder den Header oder die Daten zurück.
     * @param data Datenpacket
     * @param part 0 = Header, 1 = Daten
     * @return byte[] Packet
     */
    public static byte[] trimPacket(byte[] data, int part) {
        byte[] ret;
        byte[] header = new byte[18];
        byte[] content = new byte[data.length - 18];
        
        System.arraycopy(data, 0, header, 0, header.length);
        System.arraycopy(data, 19, content, 0, data.length - 19);

        if (part == 0) {
            ret = header;
        } else {
            ret = content;
        }
        return ret;
    }
}
