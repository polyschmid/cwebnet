package javaHisClient;

import java.net.*;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oliver, Hannes, Giulia
 */
public class HISclient {

    protected int state;
    protected int clientPort;
    protected UDPClient udp;
    protected InetAddress brokerAddress;
    protected int brokerPort;

    /**
     * Konstruktor 1
     *
     * @param mask Netzmaske
     * @param port Port
     */
    public HISclient(byte[] mask, int port) {
        initHISclient(mask, port);
    }

    /**
     * Konstruktor 2
     *
     * @param port Port
     */
    public HISclient(int port) {
        byte[] mask = new byte[4];

        for (int i = 0; i < mask.length; i++) {
            mask[i] = 0;
        }

        initHISclient(mask, port);
    }

    /**
     * Inizialisiert den Client.
     *
     * @param netmask Netzmaske
     * @param port Port
     */
    private void initHISclient(byte[] netmask, int port) {
        InetAddress myAddress; // Eigene IP(Client) nur das Objekt
        byte[] brdAddress;  // Broker Adresse (Ziel) für den Broadcast

        state = 0;
        clientPort = port; // Eigener Port

        try {
            myAddress = InetAddress.getLocalHost(); // hier holt er die eigene adresse
            System.out.println(myAddress.toString()); //geil
        } catch (UnknownHostException e) {
            myAddress = null;

            System.out.println("Unable to get ip, error: " + e.getMessage());
            System.exit(0);
        }

        //Hier wird Adresse gebaut:
        brdAddress = myAddress.getAddress();

        for (int i = 0; i < brdAddress.length; i++) {
            brdAddress[i] = (byte) (brdAddress[i] & netmask[i]);
            brdAddress[i] = (byte) (brdAddress[i] | ~netmask[i]);
        }
        System.out.print("Use broadcast address: "
                + (brdAddress[0] & 0xff) + "."
                + (brdAddress[1] & 0xff) + "."
                + (brdAddress[2] & 0xff) + "."
                + (brdAddress[3] & 0xff));
//        //UDP-Client mit Port Broadcastadresse erstellen:
        udp = new UDPClient(port, brdAddress);

    }

    /**
     * Diese Methode dient zum Broadcast ausführen und festlegen eines Brokers.
     *
     * @return int 0 für erfolgreich
     * @throws InterruptedException
     * @throws UnknownHostException
     */
    public int discover() throws InterruptedException, UnknownHostException {
        InetAddress s = InetAddress.getLocalHost();
        byte[] d = s.getAddress();
        for (int i = 0; i < d.length; i++) {
            d[i] = (byte) (d[i] & 0xff);
            d[i] = (byte) (d[i] | ~0xff);
        }
        //Port Größe nicht größer als vier stellen!
        //Header für Discover bauen:
        byte[] header = buildHeader(s, d, clientPort, 0000, 0, 1, 0, 0, 0, 0, 0);

        sendData(header);
        udp.mainLoop();
        //verarbeite empfangenes Datenpaket:
        for (int f = 0; f < udp.empfangen.size(); f++) {
            System.out.println("Länge Liste:" + udp.empfangen.size());
            DatagramPacket emp = udp.empfangen.get(f);
            if (readHeader(emp.getData(), "ack") == 1) {
                //Broker auswählen und festlegen (der erste):
                brokerAddress = emp.getAddress();
                brokerPort = emp.getPort();
                System.out.println("Broker: " + brokerAddress.toString());
                System.out.println("Port: " + brokerPort);
//                    break outerloop;
            }
        }
//        }
        return 0;
    }

    /**
     * Diese Methode ist zum Verbindungsaufbau
     *
     * @return int 0 = true, 1 = flase
     */
    public int connect() throws UnknownHostException {
        InetAddress s = InetAddress.getLocalHost();
        byte[] d = s.getAddress();
        for (int i = 0; i < d.length; i++) {
            d[i] = (byte) (d[i] & 0xff);
            d[i] = (byte) (d[i] | ~0xff);
        }
        //Port Größe nicht größer als vier Stellen!
        //Header für Join-Request wird gebaut:
        byte[] header = buildHeader(s, d, clientPort, 0000, 0, 0, 0, 1, 0, 0, 0);
        sendData(header);
        udp.mainLoop();
        //verarbeiten von empfangenen Paket
        //Wenn Join Confirm, wird erneut eine Bestätigung geschickt:
        if (readHeader(udp.receiveBuffer, "jin") == 1 && readHeader(udp.receiveBuffer, "ack") == 1) {
            header = buildHeader(s, d, clientPort, 0000, 1, 0, 0, 1, 0, 0, 0);
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Diese Methode ist zum disconnecten.
     *
     * @return int 0 = true, 1 = flase
     */
    public int disconnect() throws UnknownHostException {
        InetAddress s = InetAddress.getLocalHost();
        byte[] d = s.getAddress();
        for (int i = 0; i < d.length; i++) {
            d[i] = (byte) (d[i] & 0xff);
            d[i] = (byte) (d[i] | ~0xff);
        }
        //Header für Leave-Request:
        byte[] header = buildHeader(s, d, clientPort, 0000, 0, 0, 1, 0, 0, 0, 0);
        sendData(header);
        udp.mainLoop();
        //Wenn Leave Confirm kommt, wird noch eine Bestätigung gesendet
        if (readHeader(udp.receiveBuffer, "lev") == 1 && readHeader(udp.receiveBuffer, "ack") == 1) {
            header = buildHeader(s, d, clientPort, 0000, 1, 0, 1, 0, 0, 0, 0);
            //lösche Werte 
            brokerAddress = null;
            brokerPort = 0;
            state = 0;
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Diese Methode versendet das Packet (egal ob Discover, Data etc.)
     *
     * @param msg zu verschickenes Packet
     * @return 0 = true, 1 = false
     */
    public int sendData(byte[] msg, int rel, int cast) {
        InetAddress own = null;
        try {
            own = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(HISclient.class.getName()).log(Level.SEVERE, null, ex);
        }
        //baue Header für Datenpaket
        //berücksichtigt, was in GUI ausgewählt wurde (Broad-/Anycast, reliable/unreliable)
        byte[] header = buildHeader(own, brokerAddress, clientPort, brokerPort, 0, 0, 0, 0, cast, rel, 1);
        byte[] packet = buildPacket(msg, header);

        udp.sendData(packet);
        return 0;

    }
    /**
     * Diese Methode versendet Daten.
     * 
     * @param header
     * @return 
     */
    public int sendData(byte[] header) {
        udp.sendData(header);
        return 0;
    }

    /**
     * Diese Methode bildet ein Packet aus Header und Daten.
     *
     * @param data Daten
     * @param header Header
     * @return zusammengesetzes Byte-Array
     */
    public byte[] buildPacket(byte[] data, byte[] header) {
        byte[] datapacket = new byte[data.length + header.length];
        //zwei Arrays zusammen (Header u. Daten):
        for (int i = 0; i < (data.length + header.length); i++) {
            if (i < header.length) {
                datapacket[i] = header[i];
            } else {
                datapacket[i] = data[i - header.length];
            }
        }
        return datapacket;
    }

    /**
     * Gibt den Port Destinatin zurück aus dem Paket
     *
     * @exception Wenn der Port größer als 9999 ist kann dieser nicht ausgelesen
     * werden.
     * @param packet byte[] Packet
     * @return int Portnummer
     */
    public int getPortDestination(byte[] packet) {
        int ret = 0;
        ret = packet[8] * 100 + packet[9];
        return ret;
    }

    /**
     * Gibt den Port Source zurück aus dem Paket.
     *
     * @exception Wenn der Port größer als 9999 ist kann dieser nicht ausgelesen
     * werden.
     * @param packet byte[] Packet
     * @return int Portnummer
     */
    public static int getPortSource(byte[] packet) {
        int ret = 0;
        ret = packet[10] * 100 + packet[11];
        return ret;
    }

    /**
     * Gibt Urspungs IPAdresse zurück.
     *
     * @param packet Paket aus dem die Adresse gelesen werden soll.
     * @return InetAdress IP
     */
    public static InetAddress getIPSource(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                      (packet[0] & 0xff) + "."
                    + (packet[1] & 0xff) + "."
                    + (packet[2] & 0xff) + "."
                    + (packet[3] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestByte.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    /**
     * Gibt Ziel IPAdresse zurück.
     *
     * @param packet Paket aus dem die Adresse gelesen werden soll.
     * @return InetAdress IP
     */
    public InetAddress getIPDestination(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                      (packet[4] & 0xff) + "."
                    + (packet[5] & 0xff) + "."
                    + (packet[6] & 0xff) + "."
                    + (packet[7] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestByte.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }
    /**
     * Erstellt ein Header für ein Packet.
     *
     * @param ipS InetAddress IP Source
     * @param ipD InetAddress IP Destination
     * @param portS int Port Source (Darf nicht größer 9999 sein)
     * @param portD int Port Destination (Darf nicht größer 9999 sein)
     * @param acknowledge int 0 = kein Ack, 1 = Ack
     * @param discover int 0 = kein dis, 1 = dis
     * @param leave int 0 = kein lev, 1 = lev
     * @param join int 0 = kein jin, 1 = jin,
     * @param type int 0 = kein typ, 1 = typ
     * @param zustellung int 0 = kein zst, 1 = zst
     * @param dataPacket int 0 = kein dat, 1 = dat
     * @return byte[] header
     */
    public byte[] buildHeader(//much wow
            InetAddress ipS,
            InetAddress ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung,
            int dataPacket) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD.getAddress();
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/,
            (byte) dataPacket/*18*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }
    /**
     * Erstellt ein Header für ein Packet.
     *
     * @param ipS InetAddress IP Source
     * @param ipD byte[] IP Destination
     * @param portS int Port Source (Darf nicht größer 9999 sein)
     * @param portD int Port Destination (Darf nicht größer 9999 sein)
     * @param acknowledge int 0 = kein Ack, 1 = Ack
     * @param discover int 0 = kein dis, 1 = dis
     * @param leave int 0 = kein lev, 1 = lev
     * @param join int 0 = kein jin, 1 = jin,
     * @param type int 0 = kein typ, 1 = typ
     * @param zustellung int 0 = kein zst, 1 = zst
     * @param dataPacket int 0 = kein dat, 1 = dat
     * @return byte[] header
     */
    public byte[] buildHeader(//much wow
            InetAddress ipS,
            byte[] ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung,
            int dataPacket) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD;
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/,
            (byte) dataPacket/*18*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }

    /**
     * Ließt das Flag des Headers aus.
     * @param dataPacket Datenpaket
     * @param flag Flag welches ausgegeben werden soll. (Es gehen nur: ack,dis,lev,jin,typ,zst,dat)
     * @return  0 = false, 1 = true
     */
    public int readHeader(byte[] dataPacket, String flag) {
        int ret = 0;

        switch (flag) {
            case "ack":
                int ack = dataPacket[12];
                ret = ack;
                break;
            case "dis":
                int dis = dataPacket[13];
                ret = dis;
                break;
            case "lev":
                int lev = dataPacket[14];
                ret = lev;
                break;
            case "jin":
                int jin = dataPacket[15];
                ret = jin;
                break;
            case "typ":
                int typ = dataPacket[16];
                ret = typ;
                break;
            case "zst":
                int zst = dataPacket[17];
                ret = zst;
                break;
            case "dat":
                int dat = dataPacket[18];
                ret = dat;
                break;
        }
        return ret;
    }
    /**
     * Verändert ein Flag des Datenpaketes
     * @param dataPacket Datenpacket
     * @param flag Flag welches ausgegeben werden soll. (Es gehen nur: ack,dis,lev,jin,typ,zst,dat)
     * @param aendern 0 = false, 1 = true
     */
    public static void writeHeader(byte[] dataPacket, String flag, int aendern) {
        switch (flag) {
            case "ack":
                dataPacket[12] = (byte) aendern;
                break;
            case "dis":
                dataPacket[13] = (byte) aendern;
                break;
            case "lev":
                dataPacket[14] = (byte) aendern;
                break;
            case "jin":
                dataPacket[15] = (byte) aendern;
                break;
            case "typ":
                dataPacket[16] = (byte) aendern;
                break;
            case "zst":
                dataPacket[17] = (byte) aendern;
                break;
            case "dat":
                dataPacket[17] = (byte) aendern;
                break;
        }
    }
    /**
     * Konsolenausgabe der Flags, die im Header gesetzt wurden. 
     * Die Methode ist für Testzwecke
     * 
     * @param test1 Paket
     */
    public void sout(byte[] test1) {

        System.out.println("Ziel Port " + getPortDestination(test1));
        System.out.println("Source Port " + getPortSource(test1));
        System.out.println("Source IP " + getIPSource(test1));
        System.out.println("Destination IP " + getIPDestination(test1));
        System.out.println("Flag ack ist: " + readHeader(test1, "ack"));
        System.out.println("Flag dis ist: " + readHeader(test1, "dis"));
        System.out.println("Flag lev ist: " + readHeader(test1, "lev"));
        System.out.println("Flag jin ist: " + readHeader(test1, "jin"));
        System.out.println("Flag typ ist: " + readHeader(test1, "typ"));
        System.out.println("Flag zst ist: " + readHeader(test1, "zst"));
        System.out.println("");
        writeHeader(test1, "zst", 1);
        System.out.println("Flag zst ist: " + readHeader(test1, "zst"));

    }

}
