package javaHisClient;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Hier ist TestCode NICHT WERTEN.
 */
/**
 *
 * @author Hannes, Giulia
 */
public class TestByte_GG {
    
    public static List<byte[]> empfangen = new LinkedList<byte[]>();
    
    public static void main(String[] args) {
        try {
            yudodis();
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestByte_GG.class.getName()).log(Level.SEVERE, null, ex);
        }
        String a = "Hallo was geht";  //Eingabe
        int ack = 1; //Header Info
        int fin = 0;
        int del = 1;
        int seq = 1;
        int checksum = 161651;
        
        
        String inttostring = ""+ack+fin+del+seq+checksum;
        System.out.println("Zahlen-String: \t\t" + inttostring);
        //byte[]test = inttostring.getBytes();
        //System.out.println("getBytes: \t\t" + test.toString());
        int head = Integer.parseInt(inttostring);
        System.out.println("parseInt: \t\t" + head);
        
        
        byte[] header = intToByteArray(head);
        System.out.println("byte Array header: \t");
        for(byte b : header) {
            System.out.print(b + ", ");
        }
        

        byte[] data = a.getBytes();
        System.out.println("\nbyte Array Data: \t");
        for(byte b : data) {
            System.out.print(b + ", ");
        }
        String text_test = new String(data, StandardCharsets.UTF_8);
        System.out.println("\ntext test: \t\t" + text_test);
        byte[] datapacket = new byte[header.length + data.length];
        
        //Header und Daten in ein Array packen:
        for (int i = 0; i < (data.length + header.length); i++) { //zwei Arrays zusammen (header u. daten)
            if(i < header.length) {
                datapacket[i] = header[i];
            } else {
                datapacket[i] = data[i - header.length];
            }
        }
        System.out.println("\nbyte Header + Daten: \t");
        for(byte b : datapacket) {
            System.out.print(b + ", ");
        }
        //------------------------------------------------------
        //Bytes wieder header_newer in Zahlen umwandeln: 
        byte[] header_new = new byte[header.length];
        byte[] data_new = new byte[data.length];
        
        //header und data wieder vom datapacket trennen:
        //              (source    , start-index        , ziel-Array    , offset, länge            )
        System.arraycopy(datapacket, 0                  , header_new    , 0     , header_new.length);
        System.arraycopy(datapacket, header_new.length  , data_new      , 0     , data_new.length);
        System.out.println("\nbyte Header_new: \t");
        for(byte b : header_new) {
            System.out.print(b + ", ");
        }
        System.out.println("\nbyte Data_new: \t\t");
        for(byte b : data_new) {
            System.out.print(b + ", ");
        }
        
        int header_value = new BigInteger(header_new).intValue();
        String data_value = new String(data_new, StandardCharsets.UTF_8);
    
        System.out.println("\nwieder in Zahlen: \t" + header_value);
        System.out.println("wieder in Buchst.:\t" + data_value);
        seperate_header(header_value);
    
    }
    
    public static void yudodis() throws UnknownHostException {
        InetAddress i; 
        InetAddress ia; 
        i = InetAddress.getByName("255.255.255.255");
        ia = InetAddress.getByName("255.255.255.245");
        byte[] a = buildHeader(i, ia, 8080, 0000, 1, 0, 0, 0, 0, 0);
        empfangen.add(a);


        for(int f = 0; f < empfangen.size(); f++) {
            System.out.println("for");
            if (readHeader(empfangen.get(f), "ack") == 1) {
                /*
                Hier muss der Client den Briker choosen
                */ 
                System.out.println("sout drin yo");
                break;
            }
        }

    }
    
    public static int readHeader(byte[] dataPacket, String flag) {
        int ret = 0;

        switch (flag) {
            case "ack":
                int ack = dataPacket[12];
                ret = ack;

                break;
            case "dis":
                int dis = dataPacket[13];
                ret = dis;

                break;
            case "lev":
                int lev = dataPacket[14];
                ret = lev;

                break;
            case "jin":
                int jin = dataPacket[15];
                ret = jin;

                break;
            case "typ":
                int typ = dataPacket[16];
                ret = typ;

                break;
            case "zst":
                int zst = dataPacket[17];
                ret = zst;

                break;
        }
        return ret;
    }

    public static void writeHeader(byte[] dataPacket, String flag, int aendern) {
        switch (flag) {
            case "ack":
                dataPacket[12] = (byte) aendern;
                break;
            case "dis":
                dataPacket[13] = (byte) aendern;

                break;
            case "lev":
                dataPacket[14] = (byte) aendern;

                break;
            case "jin":
                dataPacket[15] = (byte) aendern;

                break;
            case "typ":
                dataPacket[16] = (byte) aendern;

                break;
            case "zst":
                dataPacket[17] = (byte) aendern;

                break;
        }
    }
    public static byte[] buildHeader(//much wow
            InetAddress ipS,
            InetAddress ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD.getAddress();
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }
    /**
    * Wandelt int in Byte um.
    * @param value Integer
    * @return Byte Array
    */
    public static final byte[] intToByteArray(int value) {
        return new byte[]{
            (byte) (value >>> 24),
            (byte) (value >>> 16),
            (byte) (value >>> 8),
            (byte) value};
    }
	
    public static void seperate_header(int h) {
           String all = Integer.toString(h);
           int ack_n = Character.getNumericValue(all.charAt(0));
           int fin_n = Character.getNumericValue(all.charAt(1));
           int del_n = Character.getNumericValue(all.charAt(2));
           int seq_n = Character.getNumericValue(all.charAt(3));
           int checksum_n = Integer.parseInt(all.substring(4));
           System.out.println("ack_n: \t\t" + ack_n);
           System.out.println("fin_n: \t\t" + fin_n);
           System.out.println("del_n: \t\t" + del_n);
           System.out.println("seq_n: \t\t" + seq_n);
           System.out.println("checksum_n: \t" + checksum_n);
        }
}
