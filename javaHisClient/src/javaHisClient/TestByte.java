
package javaHisClient;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.charset.StandardCharsets;

/**
 * Hier ist TestCode NICHT WERTEN.
 */
/**
 *
 * @author Hannes
 */
public class TestByte {

    public static byte[] setUpPacket(byte[] header, byte[] data) {
        byte[] ret = new byte[header.length + data.length];
        System.arraycopy(header, 0, ret, 0, header.length);
        System.arraycopy(data, 0, ret, header.length, data.length);
        return ret;
    }

    public static byte[] trimPacket(byte[] data, int part) {
        byte[] ret;
        byte[] header = new byte[18];
        byte[] content = new byte[data.length - 18];

        System.arraycopy(data, 0, header, 0, 18);
        System.arraycopy(data, 0, content, 18, data.length-18);

        if (part == 0) {
            ret = header;
        } else {
            ret = content;
        }
        return ret;
    }

    public static int getPortDestination(byte[] packet) {
        int ret = 0;
        ret = packet[8] * 100 + packet[9];
        return ret;
    }

    public static int getPortSource(byte[] packet) {
        int ret = 0;
        ret = packet[10] * 100 + packet[11];
        return ret;
    }

    public static InetAddress getIPSource(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                    (packet[0] & 0xff) + "."
                    + (packet[1] & 0xff) + "."
                    + (packet[2] & 0xff) + "."
                    + (packet[3] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestByte.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    public static InetAddress getIPDestination(byte[] packet) {
        InetAddress ret = null;
        try {
            ret = InetAddress.getByName(
                    (packet[4] & 0xff) + "."
                    + (packet[5] & 0xff) + "."
                    + (packet[6] & 0xff) + "."
                    + (packet[7] & 0xff));
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestByte.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    public static byte[] buildHeader(//much wow
            InetAddress ipS,
            InetAddress ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD.getAddress();
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }

    public byte[] buildHeader(//much wow
            InetAddress ipS,
            byte[] ipD,
            int portS,
            int portD,
            int acknowledge,
            int discover,
            int leave,
            int join,
            int type,
            int zustellung) {

        byte[] ipSource = ipS.getAddress();
        byte[] ipDestination = ipD;
        byte[] flag = {
            (byte) (portS / 100)/*8*/,
            (byte) (portS % 100)/*9*/,
            (byte) (portD / 100)/*10*/,
            (byte) (portD % 100)/*11*/,
            (byte) acknowledge/*12*/,
            (byte) discover/*13*/,
            (byte) leave/*14*/,
            (byte) join/*15*/,
            (byte) type/*16*/,
            (byte) zustellung/*17*/};
        byte[] header = new byte[ipSource.length + ipDestination.length + flag.length];
//      1.Array Source 2.Anfang Source 3.Array Desti 4.Anfang Desti 5.Länge    
        System.arraycopy(ipSource, 0, header, 0, ipSource.length);
        System.arraycopy(ipDestination, 0, header, ipSource.length, ipDestination.length);
        System.arraycopy(flag, 0, header, ipSource.length + ipDestination.length, flag.length);
        return header;
    }

    public static int readHeader(byte[] dataPacket, String flag) {
        int ret = 0;

        switch (flag) {
            case "ack":
                int ack = dataPacket[12];
                ret = ack;

                break;
            case "dis":
                int dis = dataPacket[13];
                ret = dis;

                break;
            case "lev":
                int lev = dataPacket[14];
                ret = lev;

                break;
            case "jin":
                int jin = dataPacket[15];
                ret = jin;

                break;
            case "typ":
                int typ = dataPacket[16];
                ret = typ;

                break;
            case "zst":
                int zst = dataPacket[17];
                ret = zst;

                break;
        }
        return ret;
    }

    public static void writeHeader(byte[] dataPacket, String flag, int aendern) {
        switch (flag) {
            case "ack":
                dataPacket[12] = (byte) aendern;
                break;
            case "dis":
                dataPacket[13] = (byte) aendern;

                break;
            case "lev":
                dataPacket[14] = (byte) aendern;

                break;
            case "jin":
                dataPacket[15] = (byte) aendern;

                break;
            case "typ":
                dataPacket[16] = (byte) aendern;

                break;
            case "zst":
                dataPacket[17] = (byte) aendern;

                break;
        }
    }

    public static void main(String[] args) throws UnknownHostException {
        Charset.defaultCharset();

        InetAddress ipSource = InetAddress.getByName("192.168.178.255"); //das muss auch ein string sein

        byte[] ipS = ipSource.getAddress();

        InetAddress ipDestination = InetAddress.getByName("192.168.178.81");
        byte[] ipD = ipDestination.getAddress();

        byte[] test1 = buildHeader(ipSource, ipDestination, 8040, 6070, 1, 0, 1, 0, 1, 0);

        System.out.println("Ziel Port " + getPortDestination(test1));
        System.out.println("Source Port " + getPortSource(test1));
        System.out.println("Source IP " + getIPSource(test1));
        System.out.println("Destination IP " + getIPDestination(test1));
        System.out.println("Flag ack ist: " + readHeader(test1, "ack"));
        System.out.println("Flag dis ist: " + readHeader(test1, "dis"));
        System.out.println("Flag lev ist: " + readHeader(test1, "lev"));
        System.out.println("Flag jin ist: " + readHeader(test1, "jin"));
        System.out.println("Flag typ ist: " + readHeader(test1, "typ"));
        System.out.println("Flag zst ist: " + readHeader(test1, "zst"));
        System.out.println("");
        writeHeader(test1, "zst", 1);
        System.out.println("Flag zst ist: " + readHeader(test1, "zst"));
    }

}
/**
 *
 * @author Hannes
 */
class TestStrecke {

    /**
     * Wandelt int in Byte um.
     *
     * @param value Integer
     * @return Byte Array
     */
    public static int byteArrayToInt(byte[] b) {
        return b[3] & 0xFF
                | (b[2] & 0xFF) << 8
                | (b[1] & 0xFF) << 16
                | (b[0] & 0xFF) << 24;
    }

    public static byte[] intToByteArray(int a) {
        return new byte[]{
            (byte) ((a >> 24) & 0xFF),
            (byte) ((a >> 16) & 0xFF),
            (byte) ((a >> 8) & 0xFF),
            (byte) (a & 0xFF)
        };
    }

    public static void main(String[] args) {
        byte ba[] = new byte[2];
        int a =8080;

//        ba[0] = 80;
//        ba[1] = 80;
        ba[0] = (byte) (a/100);
        ba[1] = (byte) (a%100);

        a = ba[0] * 100 + ba[1];
        System.out.println(a);

//        int portS = 255;
//        String g= ""+portS;
//        byte[] test = g.getBytes();
//
//        System.out.println(byteArrayToInt(test));
//        for(byte t:test){
//            int z = t;
//            System.out.println(z);
//        }

    }

}

