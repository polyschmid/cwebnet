
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaHisClient;

import java.io.*;
import java.net.*;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Oliver, Hannes, Giulia
 */
public class UDPClient {

    protected DatagramSocket clientSocket;
    protected InetAddress brokerAddr;
    protected int brokerPort;

    protected byte[] receiveBuffer = new byte[1024];
    protected int port;
    protected List<DatagramPacket> empfangen;

    /**
     * Konstruktor.
     *
     * @param port port
     * @param addr Broker Adresse
     */
    public UDPClient(int port, byte[] addr) {
        try {
            this.clientSocket = new DatagramSocket(port);
        } catch (SocketException se) {
            System.out.println("Unable to open socket: " + se.getMessage());
            this.clientSocket = null;
        }

        try {
            brokerAddr = InetAddress.getByAddress(addr);
        } catch (UnknownHostException e) {
            System.out.println("Error will converting IP address: " + e.getMessage());
            brokerAddr = null;
        }
    }

    @Override
    public void finalize() {
        clientSocket.close();
    }

    /**
     * Sendet Paket
     *
     * @param sendData Datenpacket.
     */
    public void sendData(byte[] sendData) {
        if (clientSocket != null) {
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, brokerAddr, 9876 /*brokerPort*/);
            try {
                clientSocket.send(sendPacket);
            } catch (IOException se) {
                System.out.println("Error will sending packet: " + se.getMessage());
            }
        }
    }

    /**
     * Verarbeitet das Empfangene Packet
     *
     * @param pRcv Das Paket was angekommen ist
     */
    protected void processPacket(DatagramPacket pRcv) {
        byte[] data = pRcv.getData(); //reine Information (Unser Objekt)

        sendData(data);
    }

    /**
     * Hier wird der Server gestartet und er erwartet nun Pakete.
     */
    public void mainLoop() {
        empfangen = new LinkedList<DatagramPacket>();
//        long endTime = System.currentTimeMillis() + (2);
        int counter = 0;
//        while (endTime > System.currentTimeMillis()) {
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        try {
            counter++;
            clientSocket.receive(receivePacket);
            empfangen.add(receivePacket);

            System.out.println(counter);
        } catch (IOException e) {
            System.out.println("IO exception: " + e.getMessage());
//                continue;
        }
        processPacket(receivePacket);
//        }
    }

}
